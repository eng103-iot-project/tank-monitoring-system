from flask import Flask, render_template, jsonify, request
from datetime import datetime
import json

app = Flask(__name__)  # App object

@app.route("/ingest", methods=["POST"])
def ingest():
    try:
        content = request.json
        print(content)
        data = open("data.json", "w")
        data.write(json.dumps(content))
        data.close()
        return jsonify({"success": True})
    except:
        return jsonify({"success": False})


@app.route("/data", methods=["GET"])
def data():
    data = open("data.json", "r")
    d = json.load(data)
    data.close()
    response = jsonify(d)
    response.headers.add("Access-Control-Allow-Origin", "*")
    response.headers.add('Access-Control-Allow-Headers', "*")
    response.headers.add('Access-Control-Allow-Methods', "*")
    return response
  
  
@app.route("/config", methods=["GET", "POST"])
def config():
    if request.method == "POST":
        content = request.form
        print(content)
        config = open("config.json", "w")
        config.write(json.dumps(content))
        config.close()
        return render_template("index.html")
    else:
        data = open("config.json", "r")
        d = json.load(data)
        print(d)
        data.close()
        return jsonify(d)

@app.route("/")
def index():
    data = open("data.json", "r")
    d = json.load(data)
    data.close()
    temperature = list(d["temp"].values())[-1]
    level = list(d["waterlevel"].values())[-1]
    timestamp = list(d["waterlevel"].keys())[-1]

    c = open("config.json", "r")
    config = json.load(c)
    c.close()
    
    config_height = int(config["tankHeight"])
    config_diameter = int(config["tankDiameter"])
    config_min_level = int(config["minWaterLevel"])
    config_max_level = int(config["maxWaterLevel"])
    config_phone_number = config["phoneNumber"]
    
    distance = round(level)
  
    if distance/100 >= config_height:
      distance = config_height*100
     
    tankVol = float(((config_diameter/2)**2 * 3.141659 * config_height)*1000)
    waterVol = float((config_diameter/2)**2 * 3.141659 * (distance / 100)*1000)
    waterVolActual = tankVol - waterVol
    
    percent = (waterVol / tankVol) * 100
    blue_line = round(percent * 2.6) # 2.6 = 260 pixels / 10
    gray_line = 260 - blue_line # 260 = 260 pixels

    temp = temperature / 0.50  # 40c max temp
    red_line = round(temp * 2.6) # 2.6 = 260 pixels / 10
    red_gray_line = 260 - red_line
    
    return render_template(
        "index.html",
        blue_line=blue_line,
        gray_line=gray_line,
        water_percent=round(100 - percent),
        red_gray_line=red_gray_line,
        red_line=red_line,
        temperature=temperature,
        timestamp=datetime.fromtimestamp(float(timestamp)).strftime("%b %d %Y %H:%M:%S"),
        config_height=config_height,
        config_diameter=config_diameter,
        config_min_level=config_min_level,
        config_max_level=config_max_level,
        config_phone_number=config_phone_number,
        data=json.dumps(d, separators=(',', ':')),
        tankVol=tankVol,
        waterVol=waterVol,
        waterVolActual=round(waterVolActual),
    )
  
app.run() 