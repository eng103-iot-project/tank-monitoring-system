import json
import requests
import RPi.GPIO as GPIO
import time
from twilio.rest import Client
import Adafruit_DHT

account_sid = "XXX"
auth_token = "XXX"
client = Client(account_sid, auth_token)

DATA_FILE = "data.json"
CONFIG_FILE = "config.json"
DUMP_URL = "https://eng103-iot-project.glitch.me/ingest"
RECIEVE_URL = "https://eng103-iot-project.glitch.me/config"
GPIO_TRIGGER = 2
GPIO_ECHO = 3
BUZZER_PIN = 26
SLEEP = 5 #aprox 16 minutes


################################ Main Function #########################################
def main():
    current_level = 0

    GPIO.setmode(GPIO.BCM)
    GPIO.setup(BUZZER_PIN, GPIO.OUT)
    data = open(DATA_FILE, 'r')
    levelData = json.load(data)
    data.close()

    settup = requests.get(RECIEVE_URL)
    content = settup.json()
    print(content)
    data = open("config.json", "w")
    data.write(json.dumps(content))
    data.close

    tankDiameter, tankHeight = content["tankDiameter"], content["tankHeight"]
    minWaterLevel, maxWaterLevel = content["minWaterLevel"], content["maxWaterLevel"]

    while True:
        counter = 0
        update_server()
        previous_level = current_level
        current_level = measurment()
        current_temp = temp_sensor()

        if current_temp >= 50:
            message = client.messages.create(
            body = "Raspberry Pi overheating!",
            from_ = "XXX",
            to = "XXX"
            )

            GPIO.output(BUZZER_PIN, True)


        if current_level >= int(content["minWaterLevel"]):
            message = client.messages.create(
            body = "Tank Empty!",
            from_ = "XXX",
            to = "XXX"
            )

            GPIO.output(BUZZER_PIN, True)

        if current_level <= int(content["maxWaterLevel"]):
            message = client.messages.create(
            body = "Tank Full!",
            from_ = "XXX",
            to = "XXX"
            )

            GPIO.output(BUZZER_PIN, True)

        if current_level <= previous_level - 1:
            counter += 1 
            if counter >= 10:
                message = client.messages.create(
                body = "Tank Leaking!",
                from_ = "XXX",
                to = "XXX"
                )

                GPIO.output(BUZZER_PIN, True)

                counter = 0

        time.sleep(SLEEP)
        GPIO.output(BUZZER_PIN, False)


def temp_sensor():

    GPIO.setmode(GPIO.BCM)

    pin = 4
    GPIO.setup(pin, GPIO.IN)


    dht_sensor = Adafruit_DHT.DHT11
    temprature = ((Adafruit_DHT.read_retry(dht_sensor, pin))[1])

    data = open(DATA_FILE, 'r')
    content = json.load(data)
    data.close()

    content["tank"]["temp"][time.time()] = temprature

    data = open(DATA_FILE, 'w')
    data.write(json.dumps(content))
    data.close()

    return temprature



################################ Send Json file to server ######################################### 
def update_server():
    data = open(DATA_FILE, 'r')
    content = json.load(data)
    data.close()
    r = requests.post(DUMP_URL, json = content["tank"])


######################################### Measures water level in cm #########################################
def measurment():
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(GPIO_TRIGGER, GPIO.OUT)
    GPIO.setup(GPIO_ECHO, GPIO.IN)

    GPIO.output(GPIO_TRIGGER, True)
    time.sleep(0.00001)
    GPIO.output(GPIO_TRIGGER, False)

    StartTime, StopTime = time.time(), time.time()

    while GPIO.input(GPIO_ECHO) == 0:
        StartTime = time.time()

    while GPIO.input(GPIO_ECHO) == 1:
        StopTime = time.time()

    TimeElapsed = StopTime - StartTime
    TimeElapsed = StopTime - StartTime
    distance = (TimeElapsed * 34300) / 2

    print(distance)

    data = open(DATA_FILE, 'r')
    content = json.load(data)
    data.close()

    content["tank"]["waterlevel"][time.time()] = distance

    data = open(DATA_FILE, 'w')
    data.write(json.dumps(content))
    data.close()

    return distance

if __name__ == "__main__":
        main()

GPIO.cleanup()


